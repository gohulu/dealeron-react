import React from "react";
import { CreateGrid } from "./components/CreateGrid/CreateGrid";

const App = () => {
  return (
    <div className="ui container">
      <CreateGrid />
    </div>
  );
};

export default App;
