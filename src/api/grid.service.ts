import { http } from "../http_commons";
import {
  IGridInputViewModel,
  IPointOutputViewModel,
} from "../types/viewmodels";

export const createGrid = async (
  data: IGridInputViewModel
): Promise<IPointOutputViewModel> => {
  return await http.post("/Grid/create", data);
};

export const moveRovers = async (
  data: string
): Promise<IPointOutputViewModel> => {
  return await http.post("/Grid/rovers/move", `"${data}"`);
};
