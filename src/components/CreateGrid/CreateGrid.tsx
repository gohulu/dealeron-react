import React, { useState, useEffect } from "react";

import { createGrid, moveRovers } from "../../api/grid.service";
import { Point } from "../../types/models/Point";
import { Direction } from "../../types/models/Direction";
import { Dimensions } from "../Dimensions";
import { GridList } from "../GridList";
import { SelectDirection } from "./SelectDirection";
import {
  IGridInputViewModel,
  IPointOutputViewModel,
} from "../../types/viewmodels";

export const CreateGrid = () => {
  const [numberOfRovers, setNumberOfRovers] = useState(5);
  const [xDimension, setXDimension] = useState(5);
  const [yDimension, setYDimension] = useState(5);
  const [direction, setDirection] = useState<Direction | undefined>(
    Direction.N
  );
  const [selectedPoint, setSelectedPoint] = useState<Point>({
    x: undefined,
    y: undefined,
  });

  useEffect(() => {}, []);
  const changeXDimension = (value: number) => {
    if (value < 1) return;

    setSelectedPoint({ x: 0, y: 0 });
    setXDimension(value);
  };

  const changeYDimension = (value: number) => {
    if (value < 1) return;

    setSelectedPoint({ x: 0, y: 0 });
    setYDimension(value);
  };

  const setFinalPoint = (
    setSelectedPoint: React.Dispatch<React.SetStateAction<Point>>,
    point: IPointOutputViewModel,
    setDirection: React.Dispatch<React.SetStateAction<Direction | undefined>>
  ) => {
    setSelectedPoint({
      x: point?.x == null ? 0 : point?.x - 1,
      y: point?.y == null ? 0 : point?.y - 1,
    });
    setDirection(point?.orientation as Direction);
  };

  const gridCreate = async (event: any) => {
    event.preventDefault();
    try {
      const data: IGridInputViewModel = {
        roverCount: numberOfRovers,
        initialX: selectedPoint.x == null ? 0 : selectedPoint.x + 1,
        initialY: selectedPoint.y == null ? 0 : selectedPoint.y + 1,
        initialOrientation: direction == null ? Direction.N : direction,
        maxHorizontalSize: xDimension,
        maxVerticalSize: yDimension,
      };
      const point = await createGrid(data);
      setFinalPoint(setSelectedPoint, point, setDirection);
    } catch (error: any) {
      console.log(error);
    }
  };

  const sendInstruction = async (event: any, data: string) => {
    event.preventDefault();
    try {
      const point = await moveRovers(data);
      setFinalPoint(setSelectedPoint, point, setDirection);
    } catch (error: any) {
      console.log(error);
    }
  };

  return (
    <>
      <h1>ROVER INSTRUCTOR</h1>
      <form className="ui form">
        <div className="field">
          <label>Number of rovers</label>
          <input
            type="number"
            name="rover-count"
            placeholder="Number of Rovers"
            value={numberOfRovers}
            onChange={(e) => setNumberOfRovers(Number.parseInt(e.target.value))}
          />
        </div>
        <Dimensions
          xDimension={xDimension}
          yDimension={yDimension}
          setXDimension={changeXDimension}
          setYDimension={changeYDimension}
        />
        <SelectDirection direction={direction} setDirection={setDirection} />
        <button className="ui primary button" onClick={gridCreate}>
          Create Grid
        </button>
        <button
          className="ui red basic button"
          onClick={(e) => sendInstruction(e, "L")}
        >
          Turn Left
        </button>
        <button
          className="ui green basic button"
          onClick={(e) => sendInstruction(e, "M")}
        >
          Move Forward
        </button>
        <button
          className="ui red basic button"
          onClick={(e) => sendInstruction(e, "R")}
        >
          Turn Right
        </button>
      </form>
      <br />
      <GridList
        x={xDimension}
        y={yDimension}
        selectedPoint={selectedPoint}
        setSelectedPoint={setSelectedPoint}
      />
      {selectedPoint &&
        `The selected point is X: ${
          selectedPoint.x == null ? 0 : selectedPoint.x + 1
        }, Y: ${
          selectedPoint.y == null ? 0 : selectedPoint.y + 1
        }, Direction: ${direction}`}
    </>
  );
};
