import React from "react";
import { Direction } from "../../types/models/Direction";

type Props = {
  direction: Direction | undefined;
  setDirection: (value: Direction) => void;
};

export const SelectDirection = ({ direction, setDirection }: Props) => {
  return (
    <div className="field">
      <label>Direction</label>
      <select
        value={direction}
        onChange={(e) => setDirection(e.target.value as Direction)}
      >
        {Object.values(Direction).map((value: Direction, index: number) => {
          return (
            <option key={index} value={value} selected={direction === value}>
              {value}
            </option>
          );
        })}
      </select>
    </div>
  );
};
