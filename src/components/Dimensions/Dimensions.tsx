import React from "react";

type Props = {
  xDimension: number;
  yDimension: number;
  setXDimension: any;
  setYDimension: any;
};

export const Dimensions = ({
  xDimension,
  yDimension,
  setXDimension,
  setYDimension,
}: Props) => {
  return (
    <div className="ui segment">
      <label>Dimensions</label>
      <div className="field">
        <label>X</label>
        <input
          type="number"
          name="dimensions"
          placeholder="X Dimension"
          value={xDimension}
          onChange={(event) => setXDimension(parseInt(event.target.value))}
        />
      </div>
      <div className="field">
        <label>Y</label>
        <input
          type="number"
          name="dimensions"
          placeholder="Y Dimension"
          value={yDimension}
          onChange={(event) => setYDimension(parseInt(event.target.value))}
        />
      </div>
    </div>
  );
};
