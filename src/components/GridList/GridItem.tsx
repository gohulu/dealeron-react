import React from "react";
import "./styles.css";

type Props = {
  selected?: boolean;
  setSelectedPoint: () => void;
};

export const GridItem = ({ selected, setSelectedPoint }: Props) => {
  return (
    <div
      className={`grid-item ${selected ? "selected" : ""}`}
      onClick={setSelectedPoint}
    />
  );
};
