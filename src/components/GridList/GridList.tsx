import React from "react";
import "./styles.css";

import { GridRow } from "./GridRow";
import { Point } from "../../types/models/Point";

type Props = {
  x: number;
  y: number;
  selectedPoint: Point;
  setSelectedPoint: (value: Point) => void;
};

export const GridList = ({ x, y, selectedPoint, setSelectedPoint }: Props) => {
  const array = Array.from(Array(y).keys());
  return (
    <>
      {array.map((value: number, index: number) => {
        return (
          <div className="row-container">
            <GridRow
              key={index}
              x={x}
              y={value}
              selectedPoint={selectedPoint}
              setSelectedPoint={setSelectedPoint}
            />
          </div>
        );
      })}
    </>
  );
};
