import React from "react";
import { Point } from "../../types/models/Point";
import { GridItem } from "./GridItem";

type Props = {
  x: number;
  y: number;
  selectedPoint: Point;
  setSelectedPoint: (value: Point) => void;
};

export const GridRow = ({ x, y, selectedPoint, setSelectedPoint }: Props) => {
  const array = Array.from(Array(x).keys());
  return (
    <>
      {array.map((value: number, index: number) => {
        const selected = selectedPoint.x === value && selectedPoint.y === y;
        return (
          <GridItem
            selected={selected}
            key={index}
            setSelectedPoint={() => setSelectedPoint({ x: value, y: y })}
          />
        );
      })}
    </>
  );
};
