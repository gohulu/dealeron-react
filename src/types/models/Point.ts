export interface Point {
  x: number | undefined;
  y: number | undefined;
}
