import IGridInputViewModel from "./input/IGridInputViewModel";
import IPointOutputViewModel from "./output/IPointOutputViewModel";

export type { IGridInputViewModel, IPointOutputViewModel };
