export default interface IGridInputViewModel {
  roverCount: number;
  initialX: number;
  initialY: number;
  initialOrientation: string;
  maxHorizontalSize: number;
  maxVerticalSize: number;
}
