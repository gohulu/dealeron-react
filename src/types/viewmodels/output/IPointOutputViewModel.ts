export default interface IPointOutputViewModel {
  x: number;
  y: number;
  orientation: string;
}
